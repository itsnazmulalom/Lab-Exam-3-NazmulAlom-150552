<html>
<head>
    <meta charset="UTF-8">
    <title>Chessboard using CSS and HTML</title>
    <style>
        .chessboard {
            width: 640px;
            height: 640px;
            margin: 20px;
            border: 10px solid #333; }
        .green {
            float: left;
            width: 80px;
            height: 80px;
            background-color: greenyellow;
            font-size:50px;
            text-align:center;
            display: table-cell;
            vertical-align:middle; }
        .Crimson {
            float: left;
            width: 80px;
            height: 80px;
            background-color: crimson;
            font-size:50px;
            text-align:center;
            display: table-cell;
            vertical-align:middle; }


    </style>
</head>
<body>
<div
    class="chessboard">

    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>

    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>

    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>

    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>

    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>

    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>

    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>

    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
    <div class="green"></div>
    <div class="crimson"></div>
</div>
</body>
</html>